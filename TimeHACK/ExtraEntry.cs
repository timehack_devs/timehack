﻿using System;
using System.Windows.Forms;

namespace TimeHACK
{
    static class ExtraEntry
    {
        internal static string gameID;
        
        // This is an extra file for testing out new Forms / apps. Do NOT edit Program.cs!
        [STAThread]
        static void Main()
        {
            // Set the GameID
            gameID = "Nightly 1.0.10";
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new WinClassicForms.WinClassicTerminal());
        }
    }
}
